# Temperature and Humidity Data logger[THDL]
THDL project monitors the temperature and relative humidity data in a warehouse environment to ensure that the goods are stored in the optimal storage conditions thereby increasing the product's shelf life resulting in increased profits.

## Project Status
- [Project Planning Sheet](https://www.google.com/url?q=https://docs.google.com/spreadsheets/d/1ayq6BoayqRruMNtnFKO3ipDIQE3Sg529AXDOp_mnxx0/edit?usp%3Dsharing&sa=D&ust=1597855682599000&usg=AFQjCNFfvmIFfDtwsz-O4BIZAHLSN5Xx0g) 

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/iotiotdotin/project-internship/temperature-and-humidity-data-logger/-/blob/master/CONTRIBUTING.md) for details on how to contribute to this project 

## Prerequisites

If you are going to contribute to code(adding features,bug fixes,testing,etc) then following are the prerequisites for it:
- Ubuntu 16.04
- Docker Engine
- Shunya OS armv7 docker image for the x86

## Installing
- [Ubuntu 16.04](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/installUbuntu)
- [Docker Engine](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/dockerBasics#docker-installation)
- [Shunyaos Docker Guide](https://hub.docker.com/r/shunyaos/shunya-armv7)

## Reporting Bugs
If you find any bugs/mistakes anywhere in this project,Please report them in this Issue[ #38](https://gitlab.com/iotiotdotin/project-internship/temperature-and-humidity-data-logger/-/issues/38)

## Discussion
If you have questions/doubts or want to make suggestions, Please use the issue [#40](https://gitlab.com/iotiotdotin/project-internship/temperature-and-humidity-data-logger/-/issues/40)

